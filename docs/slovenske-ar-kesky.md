# Zoznam slovenských AR kešiek

!!! tip
    Ak sa chceš pozrieť, kde všade sa nachádzajú slovenské AR kešky, [otvor si túto mapu](https://www.geocaching.com/map/default.aspx?asq=Ym09Qk01OTc2Ng%3d%3d).


| GC kód | Názov | Lokalita | Autor | Technológia | Publikovaná |
| ------ | ----- | -------- | ----- | ----------- | ----------- |
| [GC3N7KW](https://coord.info/GC3N7KW) | Pamätihodnosti Dúbravky | Bratislava | :ITAL | Metaverse | 16.06.2018 |
| [GC7R4KK](https://coord.info/GC7R4KK) | Back to the History: Žilina | Žilina | JuRoot | HP Reveal | 16.06.2018 |
| [GCXXXXX](https://coord.info/GCXXXXX) | TODO | Bratislava | owner | Metaverse | 16.06.2018 |
| [GCXXXXX](https://coord.info/GCXXXXX) | TODO | Bratislava | owner | Metaverse | 16.06.2018 |
| [GCXXXXX](https://coord.info/GCXXXXX) | TODO | Bratislava | owner | Metaverse | 16.06.2018 |

## Zoznam na stiahnutie

[Zoznam slovenských AR kešiek](https://www.geocaching.com/play/search?bm=BM59766) si môžeš pozrieť aj na Geocaching.com. Môžeš si tam vytvoriť Pocket Query a stiahnuť si všetky AR kešky publikované u nás.

---

Zoznam všetkých publikovaných AR kešiek na svete nájdeš cez [rozšírené vyhľadávanie na Geocaching.com](https://www.geocaching.com/play/search?ot=4&kw=AR_)[^1]. 

[^1]: Rozšírené vyhľadávanie vyžaduje Premium účet na stránkach Geocaching.com