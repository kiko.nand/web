# Často kladené otázky o HP Reveal

!!! question "Je potrebné pripojenie k internetu?"
    Aplikácia sa snaží načítať si údaje v čase, keď prebieha [príprava](priprava-aury). Avšak vzhľadom na celkové fungovanie rozoznávania obrazu a umelej inteligencie je potrebné mať prístup k internetu aj v teréne. Okrem toho môže byť súčasťou riešenia aury aj presmerovanie na webovú stránku s dodatočným obsahom. Bez pripojenia by si tak prišiel o možnosť pokračovať. Dostupnosť služieb mobilného internetu si môžeš preveriť u svojho operátora.

!!! question "Musím mať otvorenú konkrétnu auru, aby sa zobrazila?"
    Nie je to potrebné. Pokiaľ si pripravil ("followoval") profil [podľa pokynov](priprava-aury), nie je potrebné pred snímaním obrazu v aplikácii robiť ďalšie úkony. Samozrejme, pokiaľ to nie je uvedené v listingu inak.
    
!!! question "Naozaj nemám možnosť, ako sa pripojiť. Ako vyriešiť takú kešku?"
    Prvé AR kešky vznikajú v lokalitách s dobrým pokrytím signálu mobilného internetu viacerých operátorov. Ak však nemáš inú možnosť, odporúčame zahrať si hru s niekým, kto má k dispozícii pripojenie. Môžeš tiež zvážiť využitie voľne dostupných Wi-Fi sietí, ale vzhľadom na bezpečnosť a otáznu dostupnosť v spojení s často nízkou kvalitou tento postup **neodporúčame**.
    
!!! question "Koľko dát sa prenesie pri hraní cez HP Reveal?"
    Množstvo prenesených dát je závislé aj na tom, aký obsah pripravil tvorca danej aury. Hoci sa owneri snažia o čo najefektívnejšie prenosy súborov, technológia samotná umožňuje vytvorenie aury o veľkosti niekoľkých desiatok megabajtov. Rozhodne sa oplatí pripraviť si veci v zariadení na mieste, kde nemáš prísny limit prenesených dát.

!!! question "Owner informoval o novej verzii, ale ja v aplikácii vidím starý obsah. Ako ho obnovím?"
    Reveal si ukladá do pamäti telefónu taký stav Aury, aký bol pri jej prvom otvorení používateľom. Je preto potrebné ručne **zmazať dáta aplikácie** a postupovať ako [pri prvom spustení](prve-spustenie). Správny postup na zmazanie nájdeš na stránke výrobcu mobilného telefónu alebo [v pomocníkovi od Google](https://support.google.com/android).
