# Čo je HP Reveal?

HP Reveal je platforma, ktorú pamätníci AR technológie poznajú aj pod menom Aurasma. Používateľom umožňuje využívať bezplatné nástroje na prehliadanie aj tvorbu rozšírených zážitkov, pričom dôraz kladie **na rozoznávanie obrazu**.

Základ AR výtvorov založených na tomto riešení je preto v rozoznaní určitého vizuálneho prejavu **pomocou kamery mobilného telefónu**. Dokáže sa naučiť rozoznať množstvo rôznych zdrojov - od pripravených "symbolov" po pohľady na špecifické miesta v teréne. Po rozoznaní naučeného obrazu (od tvorcu konkrétnej kešky) vie na určenom mieste zobraziť obrázok, prehrať video, zvuk či 3D animáciu. Takéto vytvorené "zážitky" sa nazývajú **aury**.

Pre geocaching je vhodná aj vďaka možnosti určiť pomocou GPS lokalitu, v ktorej sa má rozoznávaný pohľad či symbol nachádzať. 