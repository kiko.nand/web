# Hra v teréne s HP Reveal

V úvode sme spomenuli, že aury v aplikácii HP Reveal sú naviazané na vizuálny vstup. Ten špecifikuje tvorca konkrétnej aury tým, že pomocou obrázku či fotky prostredia "naučí" umelú inteligenciu spoznať prvky viditeľné cez kameru mobilného telefónu.

Rozoznávanie [pripraveným zariadením](priprava-aury) sa spustí kliknutím na **modré tlačidlo dole v strede** so symbolom prerušovaného štvorca na obrazovke so zoznamom dostupných obsahov.

Počas rozoznávania sa zobrazí na displeji obraz z kamery mobilu ponad ktorý bude zobrazená animácia zväčšujúcich sa kruhov. Len čo aplikácia rozozná vo svojom výhľade známy obsah (nastavený tvorcom), animácia zmizne a zobrazí sa **obsah aury**.

!!! info "Najlepší možný obraz"
    Pre zvýšenie úspešnosti rozoznania obrazu z kamery pomôže správne prostredie. Príliš tmavé alebo presvetlené prostredie ľahko spôsobí komplikácie pri rozoznávaní. Treba si tiež dávať pozor na stopy po dotyku prstov na objektíve. Čistý vstup výrazne zvýši úspešnosť rozoznania aury.

## Typy aury

Aury môžu byť reprezentované **obrázkom**, **videom**, **zvukom** alebo **3D modelom**. 

### Všadehrateľné aury

Pokiaľ tvorca nenastavil konkrétnu lokalitu, v ktorej môže byť aura použiteľná, dá sa po príprave zobraziť **kdekoľvek**. Pre použitie je vyžadované iba namierenie kamery mobilného telefónu na správny vizuálny podnet.

### Geolokačné aury

Ak autor aury nastavil konkrétne súradnice k obrázku, na ktorý má aura reagovať, **dá sa spustiť jedine v prípade, že sa zariadenie nachádza v danej lokalite**. 

Tolerancia okolia je momentálne jednotne nastavená na rádius 50 metrov od bodu určeného tvorcom. Ak v tejto oblasti otvoríš aplikáciu s pripravenou keškou, zobrazí sa po rozoznaní obrazu príslušná aura.

## Možné akcie

Akákoľvek aura, ktorá sa zobrazí v aplikácii, je schopná reagovať na niekoľko podnetov. Záleží na autorovi, či sa to rozhodne využiť a v akej miere. 

Medzi základné akcie, ktoré môže hráč s aurou vykonať patrí:  
 - kliknutie (tapnutie) na auru  
 - dvojklik (dvojité tapnutie) na auru  
 - reakcia na rozoznanie spúšťacieho obrazu  
 - reakcia na stratu spúšťacieho obrazu z dohľadu  

Tvorcovia na tieto momenty vedia naviazať ďalšie aktivity podľa vlastnej fantázie. Určite sa oplatí pozrieť si pokyny v listingu alebo počas riešenia kešky v teréne.

## Dohral som, čo ďalej?

Len čo je tvoj AR zážitok na konci, je niekoľko možností, ako pokračovať.

**Ak si chceš zalogovať kešku**, musíš sa vydať na finálne súradnice. K nim ťa dovedú pokyny v listingu alebo v hre samotnej. Bez fyzického zápisu v logbooku nie je možné označiť kešku ako nájdenú!

Poďakovanie autorovi môžeš vysloviť nielen pekným logom, ale aj tlačidlom srdiečka v profile danej aury. Dáš tak najavo aj ďalším hráčom, že tento AR zážitok stojí za vyskúšanie a povzbudíš ownera k ďalšej tvorbe.
