disqus: geocaching-ar

# Poradňa pre HP Reveal

Ak si nenašiel riešenie svojho problému na predošlých stránkach, môžeš sa obrátiť na [oficiálne stránky podpory HP Reveal](https://aurasma.zendesk.com/), prípadne sa spýtať v komentároch nižšie. Bohužiaľ nemôžeme zabezpečiť 100% úroveň podpory (ale robíme, čo môžeme).

**Otázky ohľadom tvorby AR kešiek prosíme smerovať do [Facebookovej skupiny ownerov](../index.md#mas-chut-vytvorit-vlastnu-ar-kesku).**
