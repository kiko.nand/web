# Ako môžeš pomôcť našej príručke?

Tieto stránky sme vytvorili za účelom sprístupnenia know-how o AR keškách čo najširšiemu počtu kešerov. Snažíme sa ju tvoriť a udržovať vo svojom voľnom čase, preto uvítame každého, kto chce pridať ruku k dielu.

Príručka vzniká ako open-source riešenie pod licenciou [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0.html). Vďaka tomu má ktokoľvek s chuťou prispieť dobrej veci možnosť upraviť informácie, ktoré sa tu nachádzajú. 

Celé riešenie je postavené na technológii MkDocs s rozšírením MkDocs-Material. Zdrojový kód projektu je uchovávaný vo verejnom [repozitári na GitLabe](https://gitlab.com/gc-ar/web/).

!!! tip
    Pozri si súbor [CONTRIBUTING](https://gitlab.com/gc-ar/web/blob/master/CONTRIBUTING.md) v repozitári, kde sú základné pokyny a zdroje k úprave týchto stránok.

Repozitár a dokumentáciu spravuje [JuRoot](https://coord.info/PR1XG43). Môžeš sa naňho obrátiť v prípade záujmu o pomoc pri rozširovaní týchto stránok. Kešerský svet ti bude povďačný!
 