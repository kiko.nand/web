disqus: geocaching-ar

# Poradňa pre Metaverse

Ak si nenašiel riešenie svojho problému na predošlých stránkach, môžeš sa obrátiť na [oficiálne diskusné fórum autorov Metaverse](https://community.gometa.io/), prípadne sa spýtať v komentároch nižšie. Bohužiaľ nemôžeme zabezpečiť 100% úroveň podpory (ale robíme, čo môžeme).
