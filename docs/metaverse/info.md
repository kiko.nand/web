# Čo je Metaverse?

Platforma Metaverse dostupná na stránke [gometa.io](https://gometa.io) je produkt tvorený tímom ľudí okolo známych technologických firiem a zahraničných médií. Zameranie mnohých zamestnancov Metaverse, Inc. na storytelling je viditeľný aj v samotných možnostiach.

Metaverse ponúka hráčom možnosť zadarmo pracovať s výtvormi v augmentovanej realite. Svojimi možnosťami sa podobá na známe Wherigo kešky, avšak nejde o žiadny klon alebo úpravu. 

Okrem zobrazovania augmentovaného obsahu sa snažia tiež prepájať s rôznymi zarideniami alebo externými službami. Môžete tak cez alternatívnu realitu ovládať playlist alebo objednávať taxík.

Geocacheri tu ocenia možnosť umiestňovať rôzne AR prvky (obrázky, videá, postavičky či 3D modely) do priestoru a následne s nimi zmysluplne "pracovať" (viesť dialógy, tvoriť si inventár, sprevádzať dejom a podobne).
