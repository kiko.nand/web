# Ako vytvoriť vlastnú AR kešku?

!!! warning "Upozornenie"
    AR kešky sú momentálne publikované v rámci výnimky od Geocaching HQ ako experiment. Z toho dôvodu majú určené špecifické pravidlá, ktoré je potrebné poznať a vyžadujú viac komunikácie pri zakladaní s reviewerom.

## Základné zdroje

Predtým, ako sa pustíš do prípravy vlastnej kešky využívajúcej augmentovanú realitu, dobre si preštuduj nasledovné dokumenty:

- **[Špecifické pravidlá pre AR kešky](https://www.geocaching.com/help/index.php?pg=kb.chapter&id=127&pgid=921)** (dôležité!)  
- [Všeobecné pravidlá pre zakladanie kešiek](https://www.geocaching.com/play/guidelines)  

## Skupina pre autorov na Facebooku

Slovenskí tvorcovia AR kešiek diskutujú v skupine na Facebooku. Ak ťa zaujíma tvorba vlastnej kešky využívajúcej augmentovanú realitu, môžeš smelo [požiadať o pridanie](https://www.facebook.com/groups/438753153252013/about/).

## Výber vhodnej platformy

TODO: ako vybrať vhodnú platformu

### Metaverse

Metaverse je TODO. 

* [x] Zvláda toto
* [x] Aj toto
* [ ] ~~Toto nedáva~~

!!! tip
    Ak máš pocit, že táto aplikácia ti bude vyhovovať, pozri si [základné informácie o možnostiach pre tvorbu v Metaverse Studio](metaverse/autori/studio).

### HP Reveal

HP Reveal poskytuje TODO. 

* [x] Zvláda toto
* [x] Aj toto
* [ ] ~~Toto nedáva~~

!!! tip
    Ak chceš pre svoju AR kešku využiť práve HP Reveal, pokračuj na [základné informácie o možnostiach pre autorov v HP Reveal Studio](reveal/autori/studio).

### Tale Blazer

Aplikácia Tale Blazer vznikla na MIT TODO. 

* [x] Zvláda toto
* [x] Aj toto
* [ ] ~~Toto nedáva~~

!!! info
    Pre Tale Blazer zatiaľ nemáme spracovanú dokumentáciu. Môžeš navštíviť [oficiálne stránky aplikácie](http://www.taleblazer.org/Support/documentation), kde sa dozvieš viac o používaní. **Chceš pomôcť rozšíriť dokumentáciu? [Pozri si ako na to.](chcem-pomoct)**

### ROAR

Aplikácia ROAR slúži TODO.

* [x] Zvláda toto
* [x] Aj toto
* [ ] ~~Toto nedáva~~

!!! info
    Pre ROAR zatiaľ nemáme spracovanú dokumentáciu. Môžeš navštíviť [oficiálne stránky aplikácie](https://theroar.io/create-own-augmented-reality/), kde sa dozvieš viac o používaní. **Chceš pomôcť rozšíriť dokumentáciu? [Pozri si ako na to.](chcem-pomoct)**

## Často kladené otázky

??? question "Sú povolené aj indoor hry a stage?"
    Dobrá správa - **áno, sú** - ak sú ich výsledkom súradnice finálu, ktorý je vonku. Takže prípadné múzeá, knižnice, galérie, informačky, bane, podzemie, opustené budovy = dá sa.


## Banner do listingu

Ak chceš na tieto stránky s radami pre hráčov a tvorcov odkazovať v listingu, pripravili sme takýto banner, ktorého kód môžeš priamo použiť:

    <div style="TODO">TODO <a href="https://gc-ar.gitlab.io/web/" title="Geocaching AR" target="_blank">TODO</a></div>

Ukážka banneru:

TODO