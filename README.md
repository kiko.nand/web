#### Ak hľadáte informácie pre hráčov, [otvorte si sprievodcu kliknutím sem](https://gc-ar.gitlab.io/web/).

# AR Geocaching Web Project

Tento repozitár slúži na správu verejného projektu zameraného na informácie o augmentovanej realite v geocachingu. Prispievatelia, ktorí chcú rozšíriť informácie na stránke nájdu viac detailov v súbore [CONTRIBUTING.md](CONTRIBUTING.md).

Správca: **JuRoot** [[profil GC](https://coord.info/PR1XG43)]